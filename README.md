# CS373: Software Engineering Collatz Repo

* Name: Cheng Tan

* EID: ct28329

* GitLab ID: d8tltanc

* HackerRank ID: tancheng100

* Git SHA: d627a0f27c1d60985b72cae9f19dceaf9da21b57 

* GitLab Pipelines: https://gitlab.com/d8tltanc/cs373-collatz/pipelines

* Estimated completion time: 20 hours

* Actual completion time: 6 hours

* Comments: The "black" code formatter is making my meta cache so large to fit in the 50kb restriction of hackerrank. If I made the metacache in a single line, it would pass the submission size restriction.
